public class Solution {
    public int hammingWeight(int n) {
    //    return Integer.toBinaryString(n).replace("0","").length();        
    //    return Integer.bitCount(n);
        int ones = 0;
    	while(n!=0) {
    		ones = ones + (n & 1);
    		n = n>>>1;
    	}
    	return ones;
    }
}